<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>


## Contributing

1) The user can register. After that, he can change additional settings through the UI while working on the Vue template. Additional settings are stored in other table named Settings. And linked to another table Category.

2) Run tests
Set APP_DEBUG=true in .env file
and execute
sh phpunit-filter
or
sh phpunit-all

3) Send invitation(s) via Trustpilot
- admin user only (add admin=1 to database)
We need a token and an API key to authenticate with Trustpilot, but they don't have test API keys, so requests are now returning with an error:
Invalid access token

4) I developed the UI for TrustPilot settings. User can add settings, but not CRUD UI yet.
And also now only the current user data is sending.
