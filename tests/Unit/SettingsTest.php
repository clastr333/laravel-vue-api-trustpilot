<?php

namespace Tests\Unit;

use App\Models\Setting;
use Tests\TestCase;

class SettingsTest extends TestCase
{
    //use RefreshDatabase;

// C
    /** @test */
    public function setting_can_be_added()
    {
        if (env('APP_DEBUG')) {
            //$this->withoutExceptionHandling(); // For errors in console

            $addrrand = "Street " . rand(10, 100);
            $response = $this->post('/api/settings', [
                'user_id' => 2,
                'category_id' => 2,
                'address' => $addrrand,
                'phone' => '777888',
                'description' => 'A statement that tells you how something or someone looks, sounds, etc.'
            ]);

            $response->assertOk();
        }
    }

    /** @test */
    public function setting_fields_are_required()
    {
        if (env('APP_DEBUG')) {
            $response = $this->post('/api/settings', [
                'user_id' => '',
                'category_id' => '',
                'address' => '',
                'phone' => '',
                'description' => ''
            ]);

            $response->assertSessionHasErrors('user_id');
            $response->assertSessionHasErrors('category_id');
            $response->assertSessionHasErrors('address');
            $response->assertSessionHasErrors('phone');
            $response->assertSessionHasErrors('description');
        }
    }

// R
    /** @test */
    public function setting_can_be_shown()
    {
        if (env('APP_DEBUG')) {
            $setting1 = Setting::latest('id')->first(); // take last row
            $response = $this->get($setting1->path1());
            $response->assertOk();
        }
    }

// U
    /** @test */
    public function setting_can_be_updated()
    {
        if (env('APP_DEBUG')) {
            // Update last added
            $setting1 = Setting::latest('id')->first(); // take last row
            $addrrand_new = "Street " . rand(100, 200);
            $response = $this->put('/api/settings/' . $setting1->id, [
                'user_id' => 1,
                'category_id' => 1,
                'address' => $addrrand_new,
                'phone' => '444555',
                'description' => 'Something or someone looks, sounds, etc.'
            ]);

            $this->assertEquals(1, Setting::latest('id')->first()->user_id);
            $this->assertEquals(1, Setting::latest('id')->first()->category_id);
            $this->assertEquals($addrrand_new, Setting::latest('id')->first()->address);
            $this->assertEquals('444555', Setting::latest('id')->first()->phone);
            $this->assertEquals('Something or someone looks, sounds, etc.', Setting::latest('id')->first()->description);
        }
    }

// D
    /** @test */
    public function setting_can_be_deleted()
    {
        if (env('APP_DEBUG')) {
            // 1. Count rows
            $settingslist = Setting::all();
            $settingsCount = $settingslist->count();
            $this->assertCount($settingsCount, Setting::all());

            // 2. delete last added row
            $setting1 = Setting::latest('id')->first(); // take last row
            $response = $this->delete($setting1->path1());
            $settingsCount--;
            $this->assertCount($settingsCount, Setting::all());
        }
    }

}
