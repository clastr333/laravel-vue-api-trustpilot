<?php

namespace Tests\Unit;

use App\Models\Trustpilot;
use Tests\TestCase;

class TrustpilotsTest extends TestCase
{
    //use RefreshDatabase;

// C
    /** @test */
    public function trustpilot_can_be_added()
    {
        if (env('APP_DEBUG')) {
            //$this->withoutExceptionHandling(); // For errors in console

            $addrrand = "Street " . rand(10, 100);
            $response = $this->post('/api/trustpilots', [
                'param_name' => 'test_param',
                'param_value' => 1
            ]);

            $response->assertOk();
        }
    }

    /** @test */
    public function trustpilot_fields_are_required()
    {
        if (env('APP_DEBUG')) {
            $response = $this->post('/api/trustpilots', [
                'param_name' => '',
                'param_value' => ''
            ]);

            $response->assertSessionHasErrors('param_name');
            $response->assertSessionHasErrors('param_value');
        }
    }

// R
    /** @test */
    public function trustpilot_can_be_shown()
    {
        if (env('APP_DEBUG')) {
            $trustpilot1 = Trustpilot::latest('id')->first(); // take last row
            $response = $this->get($trustpilot1->path1());
            $response->assertOk();
        }
    }

// U
    /** @test */
    public function trustpilot_can_be_updated()
    {
        if (env('APP_DEBUG')) {
            // Update last added
            $trustpilot1 = Trustpilot::latest('id')->first(); // take last row
            $addrrand_new = "Street " . rand(100, 200);
            $response = $this->put('/api/trustpilots/' . $trustpilot1->id, [
                'param_name' => 'test_param_renamed',
                'param_value' => 2
            ]);

            $this->assertEquals('test_param_renamed', Trustpilot::latest('id')->first()->param_name);
            $this->assertEquals('2', Trustpilot::latest('id')->first()->param_value);
        }
    }

// D
    /** @test */
    public function trustpilot_can_be_deleted()
    {
        if (env('APP_DEBUG')) {
            // 1. Count rows
            $trustpilotslist = Trustpilot::all();
            $trustpilotsCount = $trustpilotslist->count();
            $this->assertCount($trustpilotsCount, Trustpilot::all());

            // 2. delete last added row
            $trustpilot1 = Trustpilot::latest('id')->first(); // take last row
            $response = $this->delete($trustpilot1->path1());
            $trustpilotsCount--;
            $this->assertCount($trustpilotsCount, Trustpilot::all());
        }
    }

}
