<?php

namespace Tests\Unit;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Support\Facades\Hash;

class UsersManagementTest extends TestCase
{
    //use RefreshDatabase;

// C
    /** @test */
    public function user_can_be_added()
    {
        if (env('APP_DEBUG')) {
            //$this->withoutExceptionHandling(); // For errors in console

            $emailrand = "email" . rand() . "@email.cm";
            $response = $this->post('/users', [
                'email' => $emailrand,
                'password' => Hash::make('Pass1Pass1'),
                'name' => 'Naam1',
                'country' => 'Belgium'
            ]);

            $response->assertOk();
        }
    }

    /** @test */
    public function user_email_and_name_are_required()
    {
        if (env('APP_DEBUG')) {
            $response = $this->post('/users', [
                'email' => '',
                'password' => Hash::make('Pass1Pass1'),
                'name' => '',
                'country' => 'Belgium'
            ]);

            $response->assertSessionHasErrors('email');
            $response->assertSessionHasErrors('name');
        }
    }

    /** @test */
    public function user_password_is_required()
    {
        if (env('APP_DEBUG')) {
            $emailrand = "email" . rand() . "@email.cm";
            $response = $this->post('/users', [
                'email' => $emailrand,
                'password' => '',
                'name' => 'Naam3',
                'country' => 'Belgium'
            ]);

            $response->assertSessionHasErrors('password');
        }
    }

// R
    /** @test */
    public function user_can_be_shown()
    {
        if (env('APP_DEBUG')) {
            $user1 = User::latest('id')->first(); // take last row
            $response = $this->get($user1->path1());
            $response->assertOk();
        }
    }

// U
    /** @test */
    public function user_can_be_updated()
    {
        if (env('APP_DEBUG')) {
            // Update last added
            $user1 = User::latest('id')->first(); // take last row
            $emailrand_new = "email" . rand() . "@email.co";
            $response = $this->put('/users/' . $user1->id, [
                'email' => $emailrand_new,
                'password' => Hash::make('Pass1Pass1'),
                'name' => 'Naam4',
                'country' => 'Poland'
            ]);

            $this->assertEquals($emailrand_new, User::latest('id')->first()->email);
            $this->assertEquals('Poland', User::latest('id')->first()->country);
        }
    }

    /** @test */
    public function user_can_be_patched()
    {
        if (env('APP_DEBUG')) {
            // Update last added
            $user1 = User::latest('id')->first(); // take last row
            $response = $this->patch('/users/' . $user1->id, [
                'country' => 'France'
            ]);

            $this->assertEquals('France', User::latest('id')->first()->country);
        }
    }

// D
    /** @test */
    public function user_can_be_deleted()
    {
        if (env('APP_DEBUG')) {
            // 1. Count rows
            $userslist = User::all();
            $usersCount = $userslist->count();
            $this->assertCount($usersCount, User::all());

            // 2. delete last added row
            $user1 = User::latest('id')->first(); // take last row
            $response = $this->delete($user1->path1());
            $usersCount--;
            $this->assertCount($usersCount, User::all());
        }
    }

}
