<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class NegativeTest extends TestCase
{
    //use RefreshDatabase;

    /** @test */
    public function any_is_not_accessable()
    {
        $responseFail = $this->get('/api/listas2');

        $responseFail->assertStatus(404);
    }
}
