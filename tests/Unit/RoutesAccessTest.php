<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ApiAccessTest extends TestCase
{
    //use RefreshDatabase;

    /** @test */
    public function users_is_accessable()
    {
        $response = $this->get('/users');

        $response->assertStatus(200);
    }
}
