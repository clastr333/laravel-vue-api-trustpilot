<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Services\ExternalToken;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
// Disable registration:
// Auth::routes(['register' => false]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/dashboard', [App\Http\Controllers\UserController::class, 'dashboard'])->middleware('auth');

Route::get('/settings', [App\Http\Controllers\SettingController::class, 'index']);
Route::get('/settings/edit', [App\Http\Controllers\SettingController::class, 'edit']);

Route::get('/trustpilots', [App\Http\Controllers\TrustpilotController::class, 'index']);
Route::get('/trustpilots/edit', [App\Http\Controllers\TrustpilotController::class, 'edit']);


// Trustpilot
Route::get('/invitations', [App\Http\Controllers\TrustpilotController::class, 'sendInvitation']);
Route::get('/invitations-t', [App\Services\ExternalToken::class, 'getToken']);

// --- User CRUD ---
// C
Route::post('/users', [App\Http\Controllers\UsersController::class, 'store']);
// R
Route::get('/users', [App\Http\Controllers\UsersController::class, 'index']);
Route::get('/users/{user}', [App\Http\Controllers\UsersController::class, 'show']);
// U
Route::put('/users/{user}', [App\Http\Controllers\UsersController::class, 'update']);
Route::patch('/users/{user}', [App\Http\Controllers\UsersController::class, 'updateField']);
// D
Route::delete('/users/{user}', [App\Http\Controllers\UsersController::class, 'destroy']);
