<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:sanctum')->get('/users', function (Request $request) {
    return $request->user();
});

Route::get('categories', [App\Http\Controllers\Api\CategoryController::class, 'index']);

Route::post('settings', [App\Http\Controllers\Api\SettingController::class, 'store']);
Route::get('settings', [App\Http\Controllers\Api\SettingController::class, 'index']);
Route::get('settings/{setting}', [App\Http\Controllers\Api\SettingController::class, 'show']);
Route::put('settings/{setting}', [App\Http\Controllers\Api\SettingController::class, 'update']);
Route::delete('settings/{setting}', [App\Http\Controllers\Api\SettingController::class, 'destroy']);

Route::post('trustpilots', [App\Http\Controllers\Api\TrustpilotController::class, 'store']);
Route::get('trustpilots', [App\Http\Controllers\Api\TrustpilotController::class, 'index']);
Route::get('trustpilots/{trustpilot}', [App\Http\Controllers\Api\TrustpilotController::class, 'show']);
Route::put('trustpilots/{trustpilot}', [App\Http\Controllers\Api\TrustpilotController::class, 'update']);
Route::delete('trustpilots/{trustpilot}', [App\Http\Controllers\Api\TrustpilotController::class, 'destroy']);
