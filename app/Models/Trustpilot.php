<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trustpilot extends Model
{
    protected $fillable = ['param_name', 'param_value'];

    public function path1()
    {
        return '/api/trustpilots/' . $this->id;
    }

    public function id()
    {
        return $this->id;
    }
}
