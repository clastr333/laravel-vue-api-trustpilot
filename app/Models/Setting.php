<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = ['category_id', 'user_id', 'address', 'phone', 'description'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function path1()
    {
        return '/api/settings/' . $this->id;
    }

    public function id()
    {
        return $this->id;
    }
}
