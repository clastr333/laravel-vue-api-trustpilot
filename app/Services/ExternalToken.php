<?php
namespace App\Services;

use App\Repository\TrustpilotRepositoryInterface;
use Illuminate\Support\Facades\Http;

class ExternalToken
{
    private $trustpilotDb;

    public function __construct(TrustpilotRepositoryInterface $trustpilotDb)
    {
        $this->trustpilotDb = $trustpilotDb;
    }

    public function getToken() : string
    {
        $requiredParams = ['grant_type', 'username', 'password', 'login_path'];
        $api_data = [];
        $login_path = '';

        $dbParamsArray = ExternalToken::getDbParams($requiredParams, '<br>', $this->trustpilotDb);
        if ( !is_array($dbParamsArray) ) {
            return $dbParamsArray;
        }

        // Fill data
        foreach ($requiredParams as $name) {
            if ($name == 'login_path') {
                $login_path = $dbParamsArray[$name];
            } else {
                $api_data[$name] = $dbParamsArray[$name];
            }
        }

        // API request
        $response = Http::post($login_path, $api_data);
        if ($response->status() == 200) {
            return json_decode($response)->token;
        } else {
            return "Can't receive token data! " . $response;
        }
    }

    public static function getDbParams($requiredParams, $devider, $trustpilotDb)
    {
        $dbParams = $trustpilotDb->selectAll();
        $lostParams = '';

        // Check if all parameters are existed in DB
        $dbParamsArray = [];
        $dbParamsNameArray = [];
        foreach ($dbParams as $dbParam) {
            $dbParamsNameArray[] = $dbParam->param_name;
            $dbParamsArray[$dbParam->param_name] = $dbParam->param_value;
        }
        foreach ($requiredParams as $name) {
            if ( !in_array($name, $dbParamsNameArray) ) {
                $lostParams .= strlen($lostParams) > 0 || $devider == "<br>"  ? $devider : '';
                $lostParams .= "'" . $name . "'";
            }
        }
        if (strlen($lostParams)) {
            return "Error: param(s) not found in DB: " . $lostParams;
        }

        return $dbParamsArray;
    }

}
