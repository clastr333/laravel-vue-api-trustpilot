<?php
namespace App\Repository;

use App\Models\Trustpilot;

class TrustpilotRepository implements TrustpilotRepositoryInterface {

    public function add($request)
    {
        return Trustpilot::create($request->validated());
    }

    public function selectAll()
    {
        return Trustpilot::all();
    }

    public function find($id)
    {
        return Trustpilot::find($id);
    }
}
