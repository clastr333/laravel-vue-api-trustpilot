<?php
namespace App\Repository;

interface TrustpilotRepositoryInterface {
    public function add($request);
    public function selectAll();
    public function find($id);
}
