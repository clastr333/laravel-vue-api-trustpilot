<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function store(Request $request)
    {
        $permission = $this->checkPermission(false, true);
        if("ok" == $permission) {
            $request->validate([
                'email' => 'required',
                'password' => 'required',
                'name' => 'required',
                'country'
            ]);
            $user = User::create($request->all());

            return response()->json([
                'message' => 'User created',
                'code' => 201,
                'user' => $user
            ]);
        } else {
            return $permission;
        }
    }

    public function index()
    {
        $permission = $this->checkPermission(false, true);
        if("ok" == $permission) {
            $users = User::all();
            return response()->json($users);
        } else {
            return $permission;
        }
    }

    public function show(User $user)
    {
        $permission = $this->checkPermission($user, false);
        if("ok" == $permission) {
            return $user;
        } else {
            return $permission;
        }
    }

    public function update(Request $request, User $user)
    {
        $permission = $this->checkPermission($user, false);
        if("ok" == $permission) {
            $request->validate([
                'email' => 'required',
                'password' => 'required',
                'name' => 'required',
                'country'
            ]);

            $user->update($request->all());

            return response()->json([
                'message' => 'User updated (put)',
                'code' => 200,
                'user' => $user
            ]);
        } else {
            return $permission;
        }
    }

    public function updateField(Request $request, User $user)
    {
        $permission = $this->checkPermission($user, false);
        if("ok" == $permission) {
            $user->update($request->all());

            return response()->json([
                'message' => 'User updated (patch)',
                'code' => 200,
                'user' => $user
            ]);
        } else {
            return $permission;
        }
    }

    public function destroy(User $user)
    {
        $permission = $this->checkPermission($user, false);
        if("ok" == $permission) {
            $user->delete();
            return response()->json([
                'message' => 'User deleted',
                'code' => 204
            ]);
        } else {
            return $permission;
        }
    }

    public static function checkPermission($user=false, $admin=false) {
      if (env('APP_DEBUG')) {
            return "ok";
        }
        if(auth()->check()) {
            if (!$admin || ($admin && auth()->user()->admin)) {
                if (!$user || ($user && $user->id() == auth()->user()->id) ) {
                    return "ok";
                } else {
                    return "You do not have access to this action! <a href='/'>Main</a>";
                }
            } else {
                return "You do not have access to this action! <a href='/'>Main</a>";
            }
        } else {
            return "You are not authenticated! <a href='/'>Main</a>";
        }
    }

}
