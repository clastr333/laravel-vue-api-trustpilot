<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Http\Requests\StoreSettingRequest;
use App\Models\Setting;

class SettingController extends Controller
{
    public function index()
    {
        $settings = Setting::with('category')->get();

        return view('settings.index', compact('settings'));
    }

    public function edit()
    {
        $categories = Category::all();
        $userId = auth()->user()->id;
        $userSettingId = Setting::where('user_id', $userId)->get(['id']);
        if (count($userSettingId) > 0) {
            $userSettingId = $userSettingId[0]->id;
        }

        return view('settings.edit', compact('categories'), compact('userSettingId'));
    }

    public function store(StoreSettingRequest $request)
    {
        Setting::create($request->validated());

        return redirect()->route('settings.index');
    }
}
