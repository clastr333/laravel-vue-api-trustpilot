<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Trustpilot;
use Illuminate\Http\Request;
use App\Http\Resources\TrustpilotResource;


class TrustpilotController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'param_name' => 'required',
            'param_value' => 'required'
        ]);
        $trustpilot = Trustpilot::create($request->all());

        return response()->json([
            'message' => 'Trustpilot created',
            'code' => 201,
            'trustpilot' => $trustpilot
        ]);
    }

    public function index()
    {
        return TrustpilotResource::collection(Trustpilot::all());
    }

    public function show(Trustpilot $trustpilot)
    {
        return [
            'id' => $trustpilot->id,
            'param_name' => $trustpilot->param_name,
            'param_value' => $trustpilot->param_value
        ];
    }

    public function update(Request $request, Trustpilot $trustpilot)
    {
        $request->validate([
            'param_name' => 'required',
            'param_value' => 'required'
        ]);

        $trustpilot->update($request->all());

        return response()->json([
            'message' => 'Trustpilot setting updated (put)',
            'code' => 200,
            'trustpilot' => $trustpilot
        ]);
    }

    public function destroy(Trustpilot $trustpilot)
    {
        $trustpilot->delete();
        return response()->json([
            'message' => 'Trustpilot setting deleted',
            'code' => 204
        ]);
    }
}
