<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Resources\SettingResource;


class SettingController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'address' => 'required',
            'phone' => 'required',
            'user_id' => 'required',
            'category_id' => 'required',
            'description' => 'required|min:2'
        ]);
        $setting = Setting::create($request->all());

        return response()->json([
            'message' => 'Setting created',
            'code' => 201,
            'setting' => $setting
        ]);

    }

    public function index()
    {
        return SettingResource::collection(Setting::all());
    }

    public function show(Setting $setting)
    {
        return [
            'id' => $setting->id,
            'user_id' => $setting->user_id,
            'category_id' => $setting->category_id,
            'address' => $setting->address,
            'phone' => $setting->phone,
            'description' => $setting->description,
        ];
    }

    public function update(Request $request, Setting $setting)
    {
        $request->validate([
            'address' => 'required',
            'phone' => 'required',
            'user_id' => 'required',
            'category_id' => 'required',
            'description' => 'required|min:2'
        ]);

        $setting->update($request->all());

        return response()->json([
            'message' => 'Settings updated (put)',
            'code' => 200,
            'setting' => $setting
        ]);
    }

    public function destroy(Setting $setting)
    {
        $setting->delete();
        return response()->json([
            'message' => 'Settings deleted',
            'code' => 204
        ]);
    }
}
