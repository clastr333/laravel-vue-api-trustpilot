<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Repository\TrustpilotRepositoryInterface;
use Illuminate\Support\Facades\Http;
use App\Http\Requests\StoreTrustpilotRequest;
use App\Services\ExternalToken;

class TrustpilotController extends Controller
{
    private $externalToken;
    private $trustpilotDb;

    public function __construct(ExternalToken $externalToken, TrustpilotRepositoryInterface $trustpilotDb)
    {
        $this->externalToken = $externalToken;
        $this->trustpilotDb = $trustpilotDb;
    }

    public function index()
    {
        $settings = $this->trustpilotDb->selectAll();

        return view('trustpilot.index', compact('settings'));
    }

    public function edit()
    {
        return view('trustpilot.edit');
    }

    public function store(StoreTrustpilotRequest $request)
    {
        $this->trustpilotDb->add($request);

        return redirect()->route('trustpilot.index');
    }

    public function sendInvitation() : string
    {
        $back = "<br><br><a href='/dashboard'>Back</a>";
        if(auth()->check()) {
            $userId = auth()->user()->id;
            $user = User::where('id', $userId)->get(['id', 'name', 'email', 'admin']);
            if (count($user) == 0) {
                return "Get User data error";
            } else {
                $token = $this->externalToken->getToken();
                if (strpos($token, "receive token data!")) {
                    return $token . $back;
                }

                $requiredParams = [
                    'businessUnitId',
                    'invitation_path',
                    'replyTo',
                    'locale',
                    'senderName',
                    'senderEmail',
                    'locationId',
                    'referenceNumber',

                    'templateId',
                    'preferredSendTime',
                    'redirectUri',
                ];
                $api_data = [];
                $lostParams = '';
                $businessUnitId = '';
                $invitation_path = '';
                $serviceReviewInvitation = [];

                $dbParamsArray = ExternalToken::getDbParams($requiredParams, '<br>', $this->trustpilotDb);
                if ( !is_array($dbParamsArray) ) {
                    return $dbParamsArray;
                }

                // Fill data
                foreach ($requiredParams as $name) {
                    switch ($name) {
                        case 'businessUnitId': $businessUnitId = $dbParamsArray[$name]; break;
                        case 'invitation_path':
                            $invitation_path = str_replace(['$','{','}'], "", $dbParamsArray[$name]);
                            $invitation_path = str_replace("businessUnitId", $businessUnitId, $invitation_path);
                            break;
                        case 'templateId';
                        case 'preferredSendTime';
                        case 'redirectUri': $serviceReviewInvitation[$name] = $dbParamsArray[$name]; break;
                        default : $api_data[$name] = $dbParamsArray[$name]; break;
                    }
                }
                $api_data['serviceReviewInvitation'] = $serviceReviewInvitation;

                // API request
                $response = Http::withToken($token)->post($invitation_path, $api_data);
                if ($response->status() == 201) {
                    return json_decode($response) . $back;
                } else {
                    return "Can't post data! " . $response . $back;
                }
            }
        } else {
            return "You are not authenticated!" . $back;
        }
    }

}
