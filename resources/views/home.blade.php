@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (auth()->check())
                        {{ __('You are logged in as') }}
                        <b>{{ auth()->user()->name }}</b>
                        ({{ auth()->user()->email }})
                    @endif

                    <br>
                    <br>
                    <a class="mobile-button btn btn-primary" href="/settings">Settings</a>
                    @if (auth()->check() && auth()->user()->admin)
                        <a class="mobile-button btn btn-secondary" href="/trustpilots">Trustpilot API Settings</a>
                        <a class="mobile-button btn btn-success" href="/invitations">Trustpilot current user invitation</a>
                        <a class="mobile-button btn btn-light" href="/invitations-t">Get token</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
