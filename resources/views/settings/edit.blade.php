@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Settings</div>

                <div class="card-body">
                    <setting-edit
                        lar-user-id="{{ auth()->user()->id }}"
                        lar-setting-id="{{ $userSettingId }}"
                    >
                    </setting-edit>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
