@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Settings List') }}</div>

                <div class="card-body">
                    <a class="btn btn-primary" href="/settings/edit">Edit settings</a>
                    <a class="btn btn-light" href="/dashboard">Back</a>
                    <br /><br />
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Address</th>
                                <th>Phone</th>
                                <th>Category</th>
                                <th>Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php($not_found = true)
                            @forelse ($settings as $setting)
                                @if ($setting->user_id == auth()->user()->id)
                                    @php($not_found = false)
                                    <tr>
                                    <td>{{ $setting->address }}</td>
                                    <td>{{ $setting->phone }}</td>
                                    <td>{{ $setting->category->name }}</td>
                                    <td>{{ $setting->created_at }}</td>
                                    </tr>
                                @endif
                            @empty
                                <tr>
                                    <td colspan="4">{{ __('No settings found') }}</td>
                                </tr>
                            @endforelse

                            @if ($not_found == true)
                            <tr>
                                <td colspan="4">{{ __('No settings found') }}</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
