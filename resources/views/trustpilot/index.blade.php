@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Settings List') }}</div>

                <div class="card-body">
                    <a class="btn btn-secondary" href="/trustpilots/edit">Add parameter</a>
                    <a class="btn btn-light" href="/dashboard">Back</a>
                    <br /><br />
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Param Name</th>
                                <th>Param Value</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($settings as $setting)
                            <tr>
                                <td>{{ $setting->param_name }}</td>
                                <td>{{ $setting->param_value }}</td>
                            <tr>
                            @empty
                            <tr>
                                <td colspan="2">{{ __('No settings found') }}</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
